#! /bin/bash
SCRIPTDIR=`dirname "${BASH_SOURCE[0]}"`
pushd ${SCRIPTDIR}
SCRIPTDIR=`pwd`
popd

../src/testEncDec
RET=$?

echo "Exiting with code ${RET}"

exit ${RET}
