/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_NUMTOHEX_HPP)
#define LIBSECRECY_NUMTOHEX_HPP

#include <cstdint>
#include <stdexcept>
#include <sstream>

namespace libsecrecy
{
	struct NumToHex
	{
		/**
		 * convert c for 0 <= c <= 15 to hex character
		 **/
		static inline char numToHex(int c)
		{
			c &= 0xF;

			if ( c < 0xA )
				return '0' + c;
			else
				return 'A' + (c-0xA);
		}

		static inline uint8_t hexToNum(char const c)
		{
			if ( c >= '0' && c <= '9' )
				return c - '0';
			else if ( c >= 'A' && c <= 'F' )
				return 0xA + (c-'A');
			else
			{
				std::ostringstream ostr;
				ostr << "libsecrecy::NumToHex::hexToNum: invalid symbol " << c;
				throw std::runtime_error(ostr.str());
			}
		}
	};
}
#endif
