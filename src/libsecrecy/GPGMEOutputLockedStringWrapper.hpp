/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_GPGMEOUTPUTLOCKEDSTRINGWRAPPER_HPP)
#define LIBSECRECY_GPGMEOUTPUTLOCKEDSTRINGWRAPPER_HPP

#include <libsecrecy/GPGMEErrorBase.hpp>
#include <libsecrecy/LockedMemory.hpp>
#include <cassert>

namespace libsecrecy
{
	struct GPGMEOutputLockedStringWrapper : public GPGMEErrorBase
	{
		std::shared_ptr< LockedMemory<char> > L;
		char * ca;
		char * ce;

		gpgme_data_cbs cbs;
		gpgme_data_t data;

		std::size_t size() const
		{
			return ca - L->begin();
		}

		void bump()
		{
			std::ptrdiff_t const diff_a = ca - L->begin();

			std::shared_ptr< LockedMemory<char> > T = L->bump();

			L = T;
			ca = L->begin()+diff_a;
			ce = L->end();
		}

		ssize_t write(char const * buffer, std::size_t size)
		{
			ssize_t copied = 0;

			while ( size )
			{
				if ( ca == ce )
				{
					try
					{
						bump();
					}
					catch(...)
					{
						return -1;
					}
				}
				else
				{
					std::ptrdiff_t const av = ce - ca;
					std::size_t const use = std::min(static_cast<std::size_t>(av),size);

					std::copy(buffer,buffer+use,ca);

					ca += use;
					buffer += use;
					size -= use;
					copied += use;
				}
			}

			return copied;
		}

		static ssize_t gpgmewrite(void * handle, void const * buffer, ::std::size_t size)
		{
			GPGMEOutputLockedStringWrapper * me = reinterpret_cast<GPGMEOutputLockedStringWrapper *>(handle);

			return me->write(reinterpret_cast<char const *>(buffer),size);
		}

		GPGMEOutputLockedStringWrapper() : L(new LockedMemory<char>(1)), ca(L->begin()), ce(L->end())
		{
			cbs.read = nullptr;
			cbs.write = gpgmewrite;
			cbs.seek = nullptr;
			cbs.release = nullptr;

			gpg_error_t const err = gpgme_data_new_from_cbs(&data,&cbs,this);

			if ( err )
			{
				throw std::runtime_error("libsecrecy::GPGMEOutputLockedStringWrapper: gpgme_data_new_from_cbs failed "+errorToString(err));
			}
		}

		~GPGMEOutputLockedStringWrapper()
		{
			gpgme_data_release(data);
		}
	};
}
#endif
